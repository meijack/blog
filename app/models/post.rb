class Post < ActiveRecord::Base
     has_many :comments, dependent: :destroy
     
     #validation
     validates_presence_of :title
     validates_presence_of :body
end
